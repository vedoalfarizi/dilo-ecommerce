//Load modules dependencies
const bodyParser = require('body-parser')
const compression = require('compression')
const cors = require('cors')
const express = require('express')
require('express-group-routes')
// const helmet = require('helmet')
// const xss = require('xss-clean')
// const rateLimit = require('express-rate-limit')
// const hpp = require('hpp')
const app = express()
const cookieParser = require('cookie-parser')
const mongoSanitize = require('express-mongo-sanitize')
const fileUpload = require('express-fileupload')
// const cloudinary = require('cloudinary').v2

const connectDB = require('./config/database')
const { requestLogger } = require('./utils/loggerHelper')

//Load Routes
const user = require('./routes/user')
const transaction = require('./routes/transaction')

const auth = require('./routes/auth')
const promo = require('./routes/promo')
const news = require('./routes/news')
const category = require('./routes/category')
const product = require('./routes/product')
const chat = require('./routes/chat')
const profile = require('./routes/profile')

const adminUser = require('./routes/admin/user')
const adminProfile = require('./routes/admin/profile')
const adminPromo = require('./routes/admin/promo')
const adminNews = require('./routes/admin/news')
const adminCategory = require('./routes/admin/category')
const adminProduct = require('./routes/admin/product')
const adminProductImage = require('./routes/admin/productImage')
const adminTransaction = require('./routes/admin/transaction')

//Load Middleware
const errorHandler = require('./middlewares/errorHandler')
// const { isLogin, authorize } = require('./middlewares/authMiddleware')

//Setup Express
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use(cookieParser())
app.use(compression())
// app.use(helmet())
// app.use(xss())

let corsOptions = {
  origin: '*',
  methods: ['*'],
  allowedHeaders: ['Content-Type']
}
app.use(cors(corsOptions))
app.use(mongoSanitize())
// app.use(hpp())

// const limiter = rateLimit({
//   windowMs: 10 * 60 * 1000,
//   max: 100
// })
// app.use(limiter)

app.use(fileUpload())

// cloudinary.config({
//     cloud_name: 'dilo-ecommerce',
//     api_key: '874837483274837',
//     api_secret: 'a676b67565c6767a6767d6767f676fe1'
// })

app.use(express.static('public'))

//Run Database Connection
connectDB()
app.use(requestLogger)

//Public Routing
app.group('/api', router => {
  router.use(auth)
  router.use(promo)
  router.use(news)
  router.use(category)
  router.use(product)
  router.use(transaction)
  router.use(profile)
})

//User Routing
app.group('/api', router => {
  // router.use(isLogin)
  // router.use(authorize('user', 'admin'))
  router.use(user)
  router.use(chat)
})

//Admin Routing
app.group('/api/admin', router => {
  // router.use(isLogin)
  // router.use(authorize('admin'))
  router.use(adminUser)
  router.use(adminProfile)
  router.use(adminPromo)
  router.use(adminNews)
  router.use(adminCategory)
  router.use(adminProduct)
  router.use(adminProductImage)
  router.use(adminTransaction)
})

//Use Middleware
app.use(errorHandler)

module.exports = app
