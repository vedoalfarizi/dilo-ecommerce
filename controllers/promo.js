const moment = require('moment')
const ErrorResponse = require('../utils/ErrorResponse')
const asyncHandler = require('../middlewares/asyncHandler')
const Promo = require('../models/Promo')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const nowDate = moment().startOf('day')

    let page = 9,
      sorting = { createdAt: 'desc' },
      filtering = {
        dueDate: {
          $gte: moment(nowDate).endOf('day')
        }
      }

    const { type } = req.query

    if (type) {
      switch (type) {
        case 'slider':
          page = 3
          break

        case 'duesoon':
          page = 1
          sorting = { dueDate: 'asc' }
          break

        case 'modal':
          page = 1
          filtering = { highlighted: true }
          break
      }
    }

    let query = Promo.find(filtering)
      .sort(sorting)
      .limit(page)

    let promos = await query.lean()

    promos.forEach(promo => {
      if (promo.startDate > moment()) {
        let status = moment(promo.startDate).fromNow()
        promo.status = `will be started ${status}`
      } else {
        let status = moment(promo.dueDate).fromNow()
        promo.status = `will be expired ${status}`
      }
    })

    res.status(200).json({
      data: promos
    })
  }),

  find: asyncHandler(async (req, res, next) => {
    const { promoId } = req.params
    const nowDate = moment().startOf('day')

    let filterQuery = null
    promoId.match(/^[0-9a-fA-F]{24}$/)
      ? (filterQuery = { _id: promoId })
      : (filterQuery = { slug: promoId })

    const promo = await Promo.findOne({
      $and: [
        filterQuery,
        {
          dueDate: {
            $gte: moment(nowDate).endOf('day')
          }
        }
      ]
    }).lean()

    if (!promo) {
      return next(new ErrorResponse(`Promo not found`, 404))
    }

    if (promo.startDate > moment()) {
      let status = moment(promo.startDate).fromNow()
      promo.status = `will be started ${status}`
    } else {
      let status = moment(promo.dueDate).fromNow()
      promo.status = `will be expired ${status}`
    }

    res.status(200).json({
      data: promo
    })
  })
}
