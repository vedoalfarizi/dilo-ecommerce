const moment = require('moment')
const ErrorResponse = require('../utils/ErrorResponse')
const asyncHandler = require('../middlewares/asyncHandler')
const News = require('../models/News')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    let news = await News.find()
      .sort({ createdAt: 'desc' })
      .lean()

    news.forEach(item => {
      item.createdAt = moment(item.createdAt).fromNow()
    })

    res.status(200).json({
      data: news
    })
  }),

  find: asyncHandler(async (req, res, next) => {
    const { newsId } = req.params

    let filterQuery = null
    newsId.match(/^[0-9a-fA-F]{24}$/)
      ? (filterQuery = { _id: newsId })
      : (filterQuery = { slug: newsId })

    let news = await News.findOne(filterQuery).lean()

    if (!news) {
      return next(new ErrorResponse(`News not found`, 404))
    }

    await News.findByIdAndUpdate(news._id, {
      $inc: { viewed: 1 }
    })

    news.viewed++
    news.createdAt = moment(news.createdAt).fromNow()

    res.status(200).json({
      data: news
    })
  }),

  search: asyncHandler(async (req, res, next) => {
    const { query } = req.body

    let news = await News.find({
      title: { $regex: `.*${query}.*`, $options: 'i' }
    }).lean()

    if (news) {
      news.forEach(item => {
        item.createdAt = moment(item.createdAt).fromNow()
      })
    }

    res.status(200).json({
      data: news
    })
  })
}
