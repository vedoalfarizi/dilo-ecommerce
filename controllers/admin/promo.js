const moment = require('moment')
const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const Promo = require('../../models/Promo')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const nowDate = moment().startOf('day')

    const promos = await Promo.find()
      .sort({ createdAt: 'desc' })
      .lean()

    promos.forEach(promo => {
      let { startDate, dueDate } = promo

      if (nowDate.isBetween(startDate, dueDate)) {
        let status = moment(dueDate).fromNow()
        promo.status = `will be expired ${status}`
      } else {
        if (nowDate > dueDate) {
          promo.status = 'expired'
        } else {
          let status = moment(startDate).fromNow()
          promo.status = `will be started ${status}`
        }
      }

      promo.startDate = moment(startDate).format('DD MMMM YYYY')
      promo.dueDate = moment(dueDate).format('DD MMMM YYYY')
    })

    res.status(200).json({
      data: promos
    })
  }),

  store: asyncHandler(async (req, res, next) => {
    await Promo.create(req.body)

    res.status(200).json({
      message: 'Promo has been successfully added'
    })
  }),

  updatePeriod: asyncHandler(async (req, res, next) => {
    const { promoId } = req.params
    const { startDate, dueDate } = req.body

    let promo = await Promo.findByIdAndUpdate(
      promoId,
      {
        startDate: startDate,
        dueDate: dueDate
      },
      {
        runValidators: true
      }
    )

    if (!promo) {
      return next(new ErrorResponse(`Promo with id ${promoId} not found`, 404))
    }

    res.status(200).json({
      message: 'Promo period has been successfully updated'
    })
  }),

  setHighlight: asyncHandler(async (req, res, next) => {
    const { promoId } = req.params

    let promo = await Promo.findById(promoId)

    if (!promo) {
      return next(new ErrorResponse(`Promo with id ${promoId} not found`, 404))
    }

    await Promo.updateMany(
      {
        highlighted: true
      },
      {
        $set: {
          highlighted: false
        }
      }
    )

    promo = await Promo.updateOne(
      { _id: promoId },
      {
        highlighted: true
      }
    )

    res.status(200).json({
      message: 'Promo has been successfully set to highlighted'
    })
  }),

  destroy: asyncHandler(async (req, res, next) => {
    const { promoId } = req.params

    let promo = await Promo.findById(promoId)

    if (!promo) {
      return next(new ErrorResponse(`Promo with id ${promoId} not found`, 404))
    }

    await promo.remove()
    res.status(200).json({
      message: 'Promo has been successfully deleted'
    })
  })
}
