const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const Category = require('../../models/Category')

module.exports = {
  getList: asyncHandler(async (req, res, next) => {
    const categories = await Category.find()
      .select('name')
      .lean()

    res.status(200).json({
      data: categories
    })
  }),

  getRoot: asyncHandler(async (req, res, next) => {
    const categories = await Category.find({ parentId: null }).populate({
      path: 'subCategories',
      select: 'name subCategories',
      populate: { path: 'subCategories', select: 'name subCategories' }
    })

    res.status(200).json({
      data: categories
    })
  }),

  store: asyncHandler(async (req, res, next) => {
    const { name } = req.body
    await Category.create({ name })

    res.status(200).json({
      message: 'Category has been successfully added'
    })
  }),

  storeSub: asyncHandler(async (req, res, next) => {
    const { categoryId } = req.params

    let categoryParent = await Category.findById(categoryId)

    if (!categoryParent) {
      return next(new ErrorResponse(`Category not found`, 404))
    }

    const subCategory = new Category({
      name: req.body.name,
      parentId: categoryId
    })

    let result = await subCategory.save()

    categoryParent.subCategories.push({
      _id: result._id
    })

    await categoryParent.save()

    res.status(200).json({
      message: 'Sub Category has been successfully added'
    })
  }),

  rename: asyncHandler(async (req, res, next) => {
    const { categoryId } = req.params
    const { name } = req.body

    const category = await Category.findByIdAndUpdate(categoryId, { name })

    if (!category) {
      return next(new ErrorResponse('Category Not Found', 404))
    }

    res.status(200).json({
      message: 'Category has been successfully updated'
    })
  }),

  destroy: asyncHandler(async (req, res, next) => {
    const { categoryId } = req.params

    let category = await Category.findById(categoryId)

    if (!category) {
      return next(new ErrorResponse('Category Not Found', 404))
    }

    //delete child by find parentId that related to this category
    await removeSubCategories(category)

    //delete subCategories in parent that related to this category
    await removeParentSubCategories(category)

    res.status(200).json({
      message: 'Category has been successfully deleted'
    })
  })
}

//helper variable for destroy category
let child = [],
  parent = []

let removeSubCategories = async category => {
  let subCategories = await Category.find({ parentId: category._id }).select(
    'subCategories'
  )

  subCategories.forEach(subCategory => {
    child.push(subCategory._id)
  })

  await Category.findByIdAndDelete(category._id)

  if (child.length > 0) {
    let categoryId = child[0]
    child.shift()
    await removeSubCategories(categoryId)
  }
}

let removeParentSubCategories = async category => {
  let parentCategories = await Category.find({ _id: category.parentId }).select(
    'parentId'
  )

  parentCategories.forEach(parentCategory => {
    parent.push(parentCategory._id)
  })

  await Category.findByIdAndUpdate(category.parentId, {
    $pull: { subCategories: category._id }
  })

  if (parent.length > 0) {
    let categoryId = parent[0]
    parent.shift()
    await removeParentSubCategories(categoryId)
  }
}
