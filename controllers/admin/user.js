const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const User = require('../../models/User')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const users = await User.find()

    res.status(200).json({
      data: users
    })
  }),

  find: asyncHandler(async (req, res, next) => {
    const { userId } = req.params

    const user = await User.findById(userId)

    if (!user) {
      return next(new ErrorResponse(`User with id ${userId} not found`, 404))
    }

    res.status(200).json({
      data: user
    })
  }),

  destroy: asyncHandler(async (req, res, next) => {
    const { userId } = req.params
    const user = await User.findByIdAndDelete(userId)

    if (!user) {
      return next(new ErrorResponse(`User not found`, 404))
    }

    res.status(200).json({
      message: 'User has been successfully deleted'
    })
  })
}
