const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const Profile = require('../../models/Profile')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const profile = await Profile.findOne().limit(1)

    if (!profile) {
      return next(new ErrorResponse('Profile not found', 404))
    }

    res.status(200).json({
      data: profile
    })
  }),

  store: asyncHandler(async (req, res, next) => {
    let profile = await Profile.find()

    if (profile.length > 0) {
      return next(new ErrorResponse('Profile is exists', 400))
    }

    profile = await Profile.create(req.body)

    res.status(200).json({
      message: 'Profile has been successfully added'
    })
  }),

  update: asyncHandler(async (req, res, next) => {
    let profile = await Profile.findOne().limit(1)

    if (profile.length > 0) {
      return next(
        new ErrorResponse(
          'Profile not found, please create a profile first',
          404
        )
      )
    }

    profile = await Profile.findByIdAndUpdate(profile._id, req.body, {
      new: true,
      runValidators: true
    })

    res.status(200).json({
      message: 'Profile has been successfully updated'
    })
  })
}
