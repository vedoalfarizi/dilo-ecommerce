const moment = require('moment')
const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const News = require('../../models/News')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    let news = await News.find()
      .sort({ createdAt: 'desc' })
      .lean()

    news.forEach(item => {
      item.createdAt = moment(item.createdAt).fromNow()
    })

    res.status(200).json({
      data: news
    })
  }),

  store: asyncHandler(async (req, res, next) => {
    await News.create(req.body)

    res.status(200).json({
      message: 'News has been successfully added'
    })
  }),

  find: asyncHandler(async (req, res, next) => {
    const { newsId } = req.params

    let filterQuery = null
    newsId.match(/^[0-9a-fA-F]{24}$/)
      ? (filterQuery = { _id: newsId })
      : (filterQuery = { slug: newsId })

    let news = await News.findOne(filterQuery)

    if (!news) {
      return next(new ErrorResponse(`News not found`, 404))
    }

    res.status(200).json({
      data: news
    })
  }),

  update: asyncHandler(async (req, res, next) => {
    const { newsId } = req.params

    let news = await News.findById(newsId)

    if (!news) {
      return next(new ErrorResponse(`News not found`, 404))
    }

    let { title, thumbnail, thumbnailDesc, body } = req.body

    news.title = title
    news.thumbnailDesc = thumbnailDesc
    news.body = body

    if (thumbnail) {
      news.thumbnail = thumbnail
    }

    await news.save()

    res.status(200).json({
      message: 'News has been successfully updated'
    })
  }),

  destroy: asyncHandler(async (req, res, next) => {
    const { newsId } = req.params

    let news = await News.findById(newsId)

    if (!news) {
      return next(new ErrorResponse(`News not found`, 404))
    }

    news.remove()
    return res.status(200).json({
      message: 'News has been successfully deleted'
    })
  })
}
