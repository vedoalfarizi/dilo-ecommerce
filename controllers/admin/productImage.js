const fs = require('fs')
const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const ProductImage = require('../../models/ProductImage')

module.exports = {
  destroy: asyncHandler(async (req, res, next) => {
    const { imageId } = req.params

    const image = await ProductImage.findById(imageId)

    if (!image) {
      return next(new ErrorResponse(`Product image not found`, 404))
    }

    const nImages = await ProductImage.find({
      productId: image.productId
    }).countDocuments()

    if (nImages <= 1) {
      return next(
        new ErrorResponse(
          `Product image can not be deleted. Each product must have at least one image`,
          400
        )
      )
    }

    await image.remove()
    res.status(200).json({
      message: 'Product image has been successfully deleted'
    })
  })
}
