const path = require('path')
const fs = require('fs')
const moment = require('moment')
const cloudinary = require('cloudinary').v2
const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const Product = require('../../models/Product')
const ProductImage = require('../../models/ProductImage')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    let products = await Product.find()
      .populate({ path: 'image', select: 'photo description -_id' })
      .populate({ path: 'categoryId', select: 'name' })
      .sort({ createdAt: 'desc' })
      .lean()

    products.forEach(product => {
      product.price = 'Rp ' + product.price.toLocaleString().replace(/,/g, '.')
      product.updatedAt = moment(product.updatedAt).fromNow()
    })

    res.status(200).json({
      data: products
    })

    //can not be use cause aggregate can not be combined wtih populate
    // let products = await Product.aggregate([
    //     {
    //         $lookup: {
    //             from: 'categories',
    //             localField: 'categoryId',
    //             foreignField: '_id',
    //             as: 'productCategory'
    //         },
    //     },
    //     {
    //         $project: {
    //             'categoryId': 0,
    //             'productCategory': {
    //                 '_id': 0,
    //                 'parentId': 0,
    //                 'subCategories': 0,
    //                 'createdAt': 0,
    //                 'updatedAt': 0,
    //                 '__v': 0
    //             }
    //         }
    //     }
    // ]) WILL BE DELETED
  }),

  find: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findById(productId)
      .populate({
        path: 'images'
      })
      .lean()

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    product.price = 'Rp ' + product.price.toLocaleString().replace(/,/g, '.')
    product.updatedAt = moment(product.updatedAt).fromNow()

    res.status(200).json({
      data: product
    })
  }),

  store: asyncHandler(async (req, res, next) => {
    let product = new Product(req.body)

    const message = 'Product has been successfully added'
    await storeImage(product, message, req, res, next)
  }),

  addProductImage: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findById(productId)

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    const message = 'Product images has been successfully added'
    await storeImage(product, message, req, res, next)
  }),

  addStock: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findByIdAndUpdate(productId, {
      $inc: {
        stock: req.body.stock
      }
    })

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    res.status(200).json({
      message: 'Product stock has been successfully added'
    })
  }),

  updateStatus: asyncHandler(async (req, res, next) => {
    const { productId } = req.params
    const { active } = req.body

    let product = await Product.findByIdAndUpdate(productId, {
      active: active
    })

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    res.status(200).json({
      message: `Product active status has been successfully updated to ${active}`
    })
  }),

  updateDiscount: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findByIdAndUpdate(productId, {
      discount: req.body.discount
    })

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    res.status(200).json({
      message: 'Product discount has been successfully updated'
    })
  }),

  update: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findByIdAndUpdate(productId, req.body)

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    res.status(200).json({
      message: 'Product has been successfully updated'
    })
  }),

  destroy: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    const product = await Product.findById(productId)

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    product.remove()

    return res.status(200).json({
      message: 'Product has been successfully deleted'
    })
  })
}

const storeImage = async (product, message, req, res, next) => {
  let desc = null
  req.body.photoDescription
    ? (desc = req.body.photoDescription)
    : (desc = req.body.description)

  let image = new ProductImage({
    productId: product._id,
    photo: req.body.photo,
    description: desc
  })

  await Promise.all([product.save(), image.save()])

  res.status(200).json({
    message: message
  })
}

// upload v2 use cloudinary
// cloudinary.uploader.upload(req.files.photo.name, async (err, result) => {
//     console.log(err)
//     if (err) {
//         return next(new ErrorResponse(`something error when uploading photo`, 500))
//     }

//     let desc = null
//     if (req.body.photoDescription) {
//         desc = req.body.photoDescription
//     } else {
//         desc = req.body.description
//     }

//     let image = new ProductImage({
//         productId: product._id,
//         photo: result.url,
//         description: desc
//     })

//     await Promise.all([product.save(), image.save()])

//     res.status(200).json({
//         message: message
//     })
// })

// destroy: asyncHandler(async (req, res, next) => {
//   const { productId } = req.params

//   const product = await Product.findById(productId)

//   if (!product) {
//     return next(new ErrorResponse('Product not found', 404))
//   }

//   delete all images based on path that related to product
//   let images = await ProductImage.find({ productId: product._id })
//     .lean()
//     .select('photo -_id')

//   deleteImage(images, err => {
//     if (err) {
//       return next(
//         new ErrorResponse('something error when deleting product image', 500)
//       )
//     } else {
//       product.remove()

//       return res.status(200).json({
//         message: 'Product has been successfully deleted'
//       })
//     }
//   })
// })

// const deleteImage = async (images, callback) => {
//   let nImage = images.length
//   images.forEach(image => {
//     fs.unlink(`${FILE_UPLOAD_PATH}/${image.photo}`, err => {
//       nImage--
//       if (err) {
//         callback(err)
//         return
//       } else if (nImage <= 0) {
//         callback(null)
//       }
//     })
//   })
// }
