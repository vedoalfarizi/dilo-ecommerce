const ErrorResponse = require('../../utils/ErrorResponse')
const asyncHandler = require('../../middlewares/asyncHandler')
const Transaction = require('../../models/Transaction')
const TransactionDetail = require('../../models/TransactionDetail')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const { status } = req.query

    let shoppingHistories = await Transaction.find()
      .sort({ createdAt: 'desc' })
      .lean()

    //filter transaction by status
    if (status) {
      switch (status) {
        case 'waited':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'waited'
          })
          break
        case 'payed':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'payed'
          })
        case 'sended':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'sended'
          })
        case 'successed':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'successed'
          })
        case 'canceled':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'canceled'
          })
      }
    }

    res.status(200).json({
      data: shoppingHistories
    })
  }),

  getDetail: asyncHandler(async (req, res, next) => {
    const { transactionId } = req.params

    const details = await TransactionDetail.find({ transactionId }).populate(
      'productId',
      'name -_id'
    )

    if (!details) {
      return next(new ErrorResponse('Transaction not found', 404))
    }

    res.status(200).json({
      data: details
    })
  }),

  updateStatus: asyncHandler(async (req, res, next) => {
    const { transactionId } = req.params
    const { action } = req.query

    if (!action && action !== 'verify' && action !== 'send') {
      return next(new ErrorResponse('Resource not found', 404))
    }

    let transaction, message
    if (action === 'send') {
      transaction = await Transaction.findOneAndUpdate(
        { _id: transactionId, status: 'payed' },
        { status: 'sended' }
      )
      message = 'Shipped!'
    } else {
      transaction = await Transaction.findOneAndUpdate(
        { _id: transactionId, status: 'waited', paymentFile: { $ne: null } },
        { $set: { status: 'payed' } }
      )
      message = 'Payment Confirmed!'
    }

    if (!transaction) {
      return next(new ErrorResponse('Transaction not valid', 400))
    }

    res.status(200).json({
      message
    })
  })
}
