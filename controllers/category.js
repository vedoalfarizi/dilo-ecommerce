const asyncHandler = require('../middlewares/asyncHandler')
const Category = require('../models/Category')
const Product = require('../models/Product')
const ErrorResponse = require('../utils/ErrorResponse')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const categories = await Category.find({ parentId: null }).populate({
      path: 'subCategories',
      select: 'name subCategories',
      populate: { path: 'subCategories', select: 'name subCategories' }
    })

    res.status(200).json({
      data: categories
    })
  }),

  getProducts: asyncHandler(async (req, res, next) => {
    const { categoryId } = req.params

    const category = await Category.findById(categoryId)

    if (!category) {
      return next(new ErrorResponse('Category not found', 404))
    }

    ;(categoriesId = []), (searchCategoryId = [])

    categoriesId.push(category._id)
    await getChildCategoryId(category._id)

    let products = await Product.find({
      categoryId: { $in: categoriesId },
      active: true
    })

    await res.status(200).json({
      data: products
    })
  })
}

let categoriesId = [],
  searchCategoryId = []

const getChildCategoryId = async category => {
  let categories = await Category.find({ parentId: category }).select(
    'subCategories'
  )

  categories.forEach(category => {
    if (category.subCategories.length > 0) {
      searchCategoryId.push(category._id)
    }
    categoriesId.push(category._id)
  })

  if (searchCategoryId.length > 0) {
    let tempId = searchCategoryId[0]
    searchCategoryId.shift()
    await getChildCategoryId(tempId)
  }
}
