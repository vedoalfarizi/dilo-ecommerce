const asyncHandler = require('../middlewares/asyncHandler')
const Transaction = require('../models/Transaction')
const TransactionDetail = require('../models/TransactionDetail')
const Product = require('../models/Product')
const User = require('../models/User')
const ErrorResponse = require('../utils/ErrorResponse')

let products = [],
  quantities = []

module.exports = {
  buy: asyncHandler(async (req, res, next) => {
    let { userId, productId, quantity } = req.body

    //to make sure that user valid
    const user = await User.findById(userId)
      .where({ role: 'user' })
      .select('name')
      .lean()

    if (!user) {
      return next(new ErrorResponse('user is not valid', 400))
    }

    //product check and save into array
    products = []
    if (typeof productId == 'string') {
      await checkProductAvailability(productId, next)
    } else {
      await Promise.all(
        productId.map(async id => checkProductAvailability(id, next))
      )
    }

    //save quantity into array
    quantities = []
    if (typeof quantity == 'string') {
      quantities.push(quantity)
    } else {
      quantity.forEach(qty => {
        quantities.push(qty)
      })
    }

    //make sure that every product have their own quantity
    if (products.length !== quantities.length) {
      return next(new ErrorResponse('transaction not valid', 400))
    }

    //create transaction to get ID
    let transaction = new Transaction({
      userId: userId
    })
    transaction = await transaction.save()

    await Promise.all(
      products.map(async product => {
        //calculate product price after discount
        let discount = product.discount
        if (discount < 100) {
          discount = Math.floor((product.price * discount) / 100)
        }
        product.price -= discount

        let qty = quantities[0]
        quantities.shift()
        let subTotal = qty * product.price

        let transactionDetail = new TransactionDetail({
          transactionId: transaction._id,
          productId: product._id,
          quantity: qty,
          subTotal
        })

        await transactionDetail.save()
      })
    )

    res.status(200).json({
      message: 'Transaction success!'
    })
  }),

  uploadPayment: asyncHandler(async (req, res, next) => {
    const { transactionId } = req.params
    const { paymentFile } = req.body

    const transaction = await Transaction.findByIdAndUpdate(transactionId, {
      paymentFile: paymentFile
    })

    if (!transaction) {
      return next(new ErrorResponse('Transaction not found', 404))
    }

    res.status(200).json({
      message: 'Payment has been successfully uploaded'
    })
  }),

  updateStatus: asyncHandler(async (req, res, next) => {
    const { transactionId } = req.params
    const { action } = req.query

    if (!action && action !== 'confirm' && action !== 'cancel') {
      return next(new ErrorResponse('Resource not found', 404))
    }

    let transaction, message
    if (action === 'confirm') {
      transaction = await Transaction.findOneAndUpdate(
        { _id: transactionId, status: 'sended' },
        { status: 'successed' }
      )
      message = 'Transaction Successed!'
    } else {
      transaction = await Transaction.findOneAndUpdate(
        { _id: transactionId, status: 'waited', paymentFile: null },
        { $set: { status: 'canceled' } }
      )
      message = 'Transaction Canceled!'
    }

    if (!transaction) {
      return next(new ErrorResponse('Transaction not valid', 400))
    }

    res.status(200).json({
      message
    })
  }),

  getTransactionDetail: asyncHandler(async (req, res, next) => {
    const { transactionId } = req.params

    const details = await TransactionDetail.find({ transactionId }).populate(
      'productId',
      'name -_id'
    )

    if (!details) {
      return next(new ErrorResponse('Transaction not found', 404))
    }

    res.status(200).json({
      data: details
    })
  })
}

let checkProductAvailability = async (productId, next) => {
  let product = await Product.findById(productId)
    .where({ active: true })
    .where('stock')
    .gt(0)
    .select('price discount')
    .lean()

  if (!product) {
    return next(new ErrorResponse('product is not valid', 400))
  }

  products.push(product)
}
