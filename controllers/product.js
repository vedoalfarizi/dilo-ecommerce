const asyncHandler = require('../middlewares/asyncHandler')
const Product = require('../models/Product')
const ErrorResponse = require('../utils/ErrorResponse')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    let products = await Product.find({ active: true, stock: { $ne: 0 } })
      .populate({
        path: 'image',
        select: 'photo description -_id'
      })
      .sort({ createdAt: 'desc' })
      .lean()

    products.forEach(async product => {
      if (product.discount) {
        await calculateDiscount(product)
      }
    })

    res.status(200).json({
      data: products
    })
  }),

  find: asyncHandler(async (req, res, next) => {
    const { productId } = req.params

    let product = await Product.findById(productId)
      .populate({
        path: 'images'
      })
      .lean()

    if (!product) {
      return next(new ErrorResponse('Product not found', 404))
    }

    await Product.findByIdAndUpdate(product._id, {
      $inc: { viewed: 1 }
    })

    if (product.discount) {
      await calculateDiscount(product)
    }

    product.viewed++
    product.price = 'Rp ' + product.price.toLocaleString().replace(/,/g, '.')

    res.status(200).json({
      data: product
    })
  })
}

const calculateDiscount = async product => {
  if (product.discount <= 100) {
    const discount = Math.floor((product.price * product.discount) / 100)
    product.discountPrice =
      'Rp ' +
      Math.floor(product.price - discount)
        .toLocaleString()
        .replace(/,/g, '.')
  } else {
    product.discountPrice =
      'Rp ' +
      (product.price - product.discount).toLocaleString().replace(/,/g, '.')
    product.discount =
      'Rp ' + product.discount.toLocaleString().replace(/,/g, '.')
  }
}
