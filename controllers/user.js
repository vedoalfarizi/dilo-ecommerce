const ErrorResponse = require('../utils/ErrorResponse')
const asyncHandler = require('../middlewares/asyncHandler')
const User = require('../models/User')
const Transaction = require('../models/Transaction')
const bcrypt = require('bcrypt')

module.exports = {
  update: asyncHandler(async (req, res, next) => {
    const { id } = req.params

    const user = await User.findByIdAndUpdate(id, req.body, {
      runValidators: true
    })

    if (!user) {
      return next(new ErrorResponse(`User not found`, 404))
    }

    res.status(200).json({
      message: 'User profile has been successfully updated'
    })
  }),

  updatePassword: asyncHandler(async (req, res, next) => {
    const { id } = req.params
    let { password, password_confirmation } = req.body

    if (!isPasswordConfirmationMatch(password, password_confirmation)) {
      return next(new ErrorResponse('Password confirmation not match', 400))
    }

    const salt = await bcrypt.genSaltSync(10)
    password = await bcrypt.hashSync(password, salt)

    await User.findByIdAndUpdate(
      id,
      { password: password },
      {
        runValidators: true
      }
    )

    res.status(200).json({
      message: 'Password has been successfully changed'
    })
  }),

  getAuthUser: asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.user.id)

    res.status(200).json({
      data: user
    })
  }),

  getTransactionHistory: asyncHandler(async (req, res, next) => {
    const { id } = req.params
    const { status } = req.query

    let shoppingHistories = await Transaction.find({ userId: id })
      .sort({ createdAt: 'desc' })
      .lean()

    //filter transaction by status
    if (status) {
      switch (status) {
        case 'waited':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'waited'
          })
          break
        case 'payed':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'payed'
          })
        case 'sended':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'sended'
          })
        case 'successed':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'successed'
          })
        case 'canceled':
          shoppingHistories = shoppingHistories.filter(history => {
            return history.status === 'canceled'
          })
      }
    }

    res.status(200).json({
      data: shoppingHistories
    })
  })
}

const isPasswordConfirmationMatch = (password, rePassword) => {
  return password === rePassword ? true : false
}
