const asyncHandler = require('../middlewares/asyncHandler')
const Chat = require('../models/Chat')
const Message = require('../models/Message')

module.exports = {
  getChatBox: asyncHandler(async (req, res, next) => {
    const { userId, adminId } = req.body

    let chats = await Chat.findOne({
      userId: userId,
      adminId: adminId
    }).populate({ path: 'histories', select: 'senderId message createdAt' })

    if (!chats) {
      chats = await Chat.create(req.body)
    }

    res.status(200).json({
      data: chats
    })
  }),

  sendMessage: asyncHandler(async (req, res, next) => {
    await Message.create(req.body)

    res.status(200).json({
      message: 'message has been successfully sended'
    })
  })
}
