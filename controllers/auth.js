const { APP_ENV, COOKIE_EXPIRE } = require('../config/app')
const ErrorResponse = require('../utils/ErrorResponse')
const asyncHandler = require('../middlewares/asyncHandler')
const User = require('../models/User')

module.exports = {
  register: asyncHandler(async (req, res, next) => {
    const { password, password_confirmation } = req.body

    if (!isPasswordConfirmationMatch(password, password_confirmation)) {
      return next(new ErrorResponse('Password does not match', 400))
    }

    await User.create(req.body)

    res.status(200).json({
      message: 'Registration success'
    })
  }),

  login: asyncHandler(async (req, res, next) => {
    const { credential, password } = req.body
    const user = await User.findOne()
      .or([
        { username: credential },
        { email: credential },
        { phone: credential }
      ])
      .select('+password')

    if (!user) {
      return next(
        new ErrorResponse('Invalid credential, wrong username/email/phone', 401)
      )
    }

    const isValidPassword = await user.matchPassword(password)
    if (!isValidPassword) {
      return next(new ErrorResponse('Invalid credential, wrong password', 401))
    }
    const token = user.getSignedJwtToken()

    //cookie setting
    const options = {
      expires: new Date(Date.now() + COOKIE_EXPIRE * 24 * 60 * 60 * 1000),
      httpOnly: true
    }

    if (APP_ENV === 'production') {
      options.secure = true
    }

    res
      .status(200)
      .cookie('token', token, options)
      .json({
        role: user.role,
        token: token
      })
  }),

  logout: asyncHandler(async (req, res, next) => {
    res.cookie('token', 'none', {
      expires: new Date(Date.now() + 10 * 1000),
      httpOnly: true
    })

    res.status(200).json({
      message: 'Logout Success!'
    })
  })
}

const isPasswordConfirmationMatch = (password, rePassword) => {
  return password === rePassword ? true : false
}
