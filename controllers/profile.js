const asyncHandler = require('../middlewares/asyncHandler')
const Profile = require('../models/Profile')

module.exports = {
  get: asyncHandler(async (req, res, next) => {
    const profile = await Profile.findOne()
      .sort({ createdAt: 'desc' })
      .select('email description phone -_id')

    res.status(200).json({
      data: profile
    })
  })
}
