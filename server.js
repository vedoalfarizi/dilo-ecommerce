//Load Config
const { APP_NAME, APP_URL, PORT } = require('./config/app')
const { logger } = require('./utils/loggerHelper')

const app = require('./app')

//Run Server
const server = app.listen(PORT, () => {
  logger.info(`${APP_NAME} app listening on ${APP_URL}!`)
})

//Error handling for promise rejection
process.on('unhandledRejection', (err, promise) => {
  logger.error(`Error on promise cause ${err.message}`)
  // server.close(() => process.exit(1)) //stop server
})
