const chai = require('chai')
const expect = chai.expect
const sinon = require('sinon')

const authController = require('../../controllers/auth')

const user = {
  body: (username, email, gender, phone, password, password_confirmation) => {
    ;(this.username = username),
      (this.email = email),
      (this.gender = gender),
      (this.phone = phone),
      (this.password = password),
      (this.password_confirmation = password_confirmation)
  }
}

describe('authController', function() {
  describe('register', function() {
    it('should return 200 if registration success', function() {
      let req = user

      let res = {
        status: sinon.spy(),
        message: sinon.spy()
      }
      let next = {}

      authController.register(req, res, next)
      expect(res.message.calledOnce)
    })
  })
})
