const ErrorResponse = require('../utils/ErrorResponse')

const schemaMiddleware = schema => {
  return (req, res, next) => {
    const { error } = schema(req.body)

    if (error) {
      const messages = Object.values(error.details).map(val => val.message)
      return next(new ErrorResponse(messages, 400))
    } else {
      return next()
    }
  }
}

module.exports = schemaMiddleware
