const ErrorResponse = require('../utils/ErrorResponse')
const { logger } = require('../utils/loggerHelper')

const errorHandler = (err, req, res, next) => {
  let error = { ...err }

  error.message = err.message

  if (err.name === 'CastError') {
    const message = `Resource not found`
    error = new ErrorResponse(message, 404)
  }

  if (err.code === 11000) {
    const message = 'Duplicate field value entered'
    const fields = err.errmsg.split('"')
    error = new ErrorResponse(
      `${message} ${fields[fields.length - 2] || ''}`,
      400
    )
  }

  if (err.name === 'ValidationError') {
    const message = Object.values(err.errors).map(val => val.message)
    error = new ErrorResponse(message, 400)
  }

  if (error.statusCode >= 500) {
    logger.error(error.message)
  }

  res.status(error.statusCode || 500).json({
    message: error.message || 'Server Error'
  })
}

module.exports = errorHandler
