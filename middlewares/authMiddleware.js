const jwt = require('jsonwebtoken')
const asyncHandler = require('./asyncHandler')
const ErrorResponse = require('../utils/ErrorResponse')
const { JWT_SECRET } = require('../config/app')
const User = require('../models/User')

module.exports = {
  isLogin: asyncHandler(async (req, res, next) => {
    let token =
      req.headers['x-access-token'] ||
      req.headers['authorization'] ||
      req.cookies.token

    if (token) {
      if (token.startsWith('Bearer')) {
        token = token.slice(7, token.length)
      }

      jwt.verify(token, JWT_SECRET, (err, authenticated) => {
        if (err) {
          return next(
            new ErrorResponse(
              'Not authorize to access this route, token not valid',
              401
            )
          )
        } else {
          User.findById(authenticated.id, (err, user) => {
            if (err) {
              return next(
                new ErrorResponse('Not authorized to access this route', 401)
              )
            }

            req.user = user

            next()
          })
        }
      })
    } else {
      return next(new ErrorResponse('Not authorized to access this route', 401))
    }
  }),

  authorize: (...roles) => {
    return (req, res, next) => {
      if (!roles.includes(req.user.role)) {
        return next(
          new ErrorResponse(
            `Forbidden, ${req.user.role} not authorized to access this route`,
            403
          )
        )
      }

      next()
    }
  }
}
