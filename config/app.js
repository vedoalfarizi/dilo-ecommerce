const dotenv = require('dotenv')
dotenv.config()

//Load all env variable and set alias
module.exports = {
  APP_NAME: process.env.APP_NAME,
  APP_URL: process.env.APP_URL,
  APP_ENV: process.env.APP_ENV,
  MONGODB_URI: process.env.MONGODB_URI,
  JWT_SECRET: process.env.JWT_SECRET,
  JWT_EXPIRE: process.env.JWT_EXPIRE,
  COOKIE_EXPIRE: process.env.COOKIE_EXPIRE,
  PORT: process.env.PORT || 5000,
  FILE_UPLOAD_PATH: process.env.FILE_UPLOAD_PATH,
  MAX_FILE_UPLOAD: process.env.MAX_FILE_UPLOAD || 1000000,
  FILE_GET_PATH: `${process.env.APP_URL}${process.env.FILE_GET_PATH}`
}
