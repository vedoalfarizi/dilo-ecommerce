const mongoose = require('mongoose')
const { MONGODB_URI } = require('./app')
const { logger } = require('../utils/loggerHelper')

const connectDB = async () => {
  const conn = await mongoose.connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  })

  logger.info(`DB connected to ${conn.connection.host}`)
}

module.exports = connectDB
