const Joi = require('@hapi/joi')

const updateProfileSchema = body => {
  const schema = Joi.object({
    username: Joi.string().required(),
    email: Joi.string().required(),
    gender: Joi.string().required(),
    phone: Joi.string().required()
  })

  return schema.validate(body)
}

module.exports = updateProfileSchema
