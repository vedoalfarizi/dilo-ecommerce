const Joi = require('@hapi/joi')

const registerSchema = body => {
  const schema = Joi.object({
    username: Joi.string()
      .min(4)
      .max(16)
      .required(),
    email: Joi.string().required(),
    gender: Joi.string().required(),
    phone: Joi.string().required(),
    password: Joi.string().required(),
    password_confirmation: Joi.ref('password')
  })

  return schema.validate(body)
}

module.exports = registerSchema
