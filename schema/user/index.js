const registerSchema = require('./userRegister')
const updateProfileSchema = require('./updateProfile')

module.exports = { registerSchema, updateProfileSchema }
