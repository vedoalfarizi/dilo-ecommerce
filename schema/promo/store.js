const Joi = require('@hapi/joi')

const storePromoSchema = body => {
  const schema = Joi.object({
    name: Joi.string().required(),
    banner: Joi.string().required(),
    startDate: Joi.date().required(),
    dueDate: Joi.date().required()
  })

  return schema.validate(body)
}

module.exports = storePromoSchema
