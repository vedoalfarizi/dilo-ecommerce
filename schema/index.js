const users = require('./user')
const news = require('./news')
const promo = require('./promo')

const schemas = {
  ...users,
  ...news,
  ...promo
}

module.exports = schemas
