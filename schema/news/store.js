const Joi = require('@hapi/joi')

const storeNewsSchema = body => {
  const schema = Joi.object({
    title: Joi.string().required(),
    thumbnail: Joi.string().required(),
    thumbnailDesc: Joi.string().required(),
    body: Joi.string().required()
  })

  return schema.validate(body)
}

module.exports = storeNewsSchema
