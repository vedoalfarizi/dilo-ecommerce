const mongoose = require('mongoose')
const { logger } = require('../utils/loggerHelper')

const TransactionDetail = mongoose.Schema(
  {
    transactionId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'transactions',
      required: [true, 'please do transaction first']
    },
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'products',
      required: [true, 'at least you must choose one product to buy']
    },
    quantity: {
      type: Number,
      required: [true, 'please insert a quantity']
    },
    subTotal: Number
  },
  { timestamps: true }
)

TransactionDetail.post('save', function() {
  this.constructor.updateProductSold(this.productId, this.quantity)
  this.constructor.setTotalTransaction(this.transactionId)
})

TransactionDetail.pre('remove', function() {
  this.constructor.updateProductSold(this.productId, this.quantity, true)
})

//update product sold value, include product Stock
TransactionDetail.statics.updateProductSold = async function(
  productId,
  quantity,
  deleting = false
) {
  const history = await this.aggregate([
    {
      $match: { productId }
    },
    {
      $group: {
        _id: '$productId',
        amountSold: { $sum: '$quantity' }
      }
    }
  ])

  let stockFactor = -1
  if (deleting) {
    stockFactor = 1
  }

  try {
    await this.model('products').findByIdAndUpdate(productId, {
      $set: { sold: history[0].amountSold },
      $inc: { stock: stockFactor * quantity }
    })
  } catch (err) {
    logger.error(err)
  }
}

//update transaction total value
TransactionDetail.statics.setTotalTransaction = async function(transactionId) {
  const history = await this.aggregate([
    {
      $match: { transactionId }
    },
    {
      $group: {
        _id: '$transactionId',
        total: { $sum: '$subTotal' }
      }
    }
  ])

  try {
    await this.model('transactions').findByIdAndUpdate(transactionId, {
      total: history[0].total
    })
  } catch (err) {
    logger.error(err)
  }
}

module.exports = mongoose.model('transactiondetails', TransactionDetail)
