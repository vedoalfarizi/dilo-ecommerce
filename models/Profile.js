const mongoose = require('mongoose')

const Profile = mongoose.Schema({
    phone: {
        type: String,
        required: [true, 'please insert a phone number'],
        match: [/^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}/,
            'please input a valid phone number']
    },
    email: {
        type: String,
        required: [true, 'please insert an email'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'please input a valid email']
    },
    description: String
})

module.exports = mongoose.model('profiles', Profile)