const mongoose = require('mongoose')
const slugify = require('slugify')
const moment = require('moment')

const Promo = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'please insert a name']
    },
    slug: {
      type: String,
      unique: [true, 'slug is exist, try another name']
    },
    banner: {
      type: String,
      required: [true, 'please insert a banner']
    },
    highlighted: {
      type: Boolean,
      default: false
    },
    startDate: {
      type: Date,
      required: [true, 'please insert the beginning of promo date']
    },
    dueDate: {
      type: Date,
      required: [true, 'please insert the end of promo date']
    }
  },
  { timestamps: true }
)

//generate slug by name
Promo.pre('save', async function(next) {
  this.slug = slugify(this.name, {
    replacement: '-',
    remove: /[*+~.()'"!:@]/g,
    lower: true
  })

  this.dueDate = moment(this.dueDate).endOf('day')

  next()
})

module.exports = mongoose.model('promos', Promo)
