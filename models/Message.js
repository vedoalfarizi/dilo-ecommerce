const mongoose = require('mongoose')

const Message = mongoose.Schema(
  {
    chatId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'chats',
      require: [true, 'chat ID can not be null']
    },
    senderId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      require: [true, 'chat ID can not be null']
    },
    message: String
  },
  { timestamps: true }
)

module.exports = mongoose.model('messages', Message)
