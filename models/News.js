const mongoose = require('mongoose')
const slugify = require('slugify')

const News = mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, 'please insert a title']
    },
    slug: {
      type: String,
      unique: [true, 'slug is exist, try another name']
    },
    thumbnail: {
      type: String,
      required: [true, 'please insert a thumbnail']
    },
    thumbnailDesc: {
      type: String,
      required: [true, 'please insert a thumbnail description']
    },
    body: {
      type: String,
      required: [true, 'please insert content of news']
    },
    viewed: {
      type: Number,
      default: 0
    }
  },
  { timestamps: true }
)

//generate slug by title
News.pre('save', async function(next) {
  this.slug = slugify(this.title, {
    replacement: '-',
    remove: /[*+~.()'"!:@]/g,
    lower: true
  })
  next()
})

module.exports = mongoose.model('news', News)
