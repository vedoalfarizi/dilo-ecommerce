const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const { JWT_SECRET, JWT_EXPIRE } = require('../config/app')

const User = mongoose.Schema(
  {
    username: {
      type: String,
      unique: [true, 'this username has been taken'],
      match: [/^[a-z0-9_-]{4,16}$/, 'please input a valid username'],
      required: [true, 'please input a username'],
      minlength: [4, 'username can not be less than 4 characters'],
      maxlength: [16, 'username can not be more than 16 characters']
    },
    email: {
      type: String,
      unique: [true, 'this email has been taken'],
      match: [
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        'please input a valid email'
      ],
      required: [true, 'please input an email']
    },
    gender: {
      type: String,
      enum: ['Laki-laki', 'Perempuan'],
      required: [true, 'please select your gender']
    },
    phone: {
      type: String,
      unique: [true, 'this phone number has been taken'],
      match: [
        /^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}/,
        'please input a valid phone number'
      ],
      required: [true, 'please input a phone number']
    },
    password: {
      type: String,
      minlength: [6, 'Password can not be less than 6 characters'],
      required: [true, 'please input a password'],
      select: false //to make password hidden
    },
    role: {
      type: String,
      enum: ['user', 'admin'],
      default: 'user'
    },
    fullname: String,
    birthDate: Date,
    bankAccount: {
      type: Number
    }
  },
  { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } }
)

//password encryption process before save the data
User.pre('save', async function(next) {
  if (!this.isModified('password')) {
    next()
  }

  const salt = await bcrypt.genSaltSync(10)
  this.password = await bcrypt.hashSync(this.password, salt)
})

//generate token
User.methods.getSignedJwtToken = function() {
  return jwt.sign(
    {
      id: this._id,
      email: this.email,
      role: this.role
    },
    JWT_SECRET,
    { expiresIn: JWT_EXPIRE }
  )
}

//password check
User.methods.matchPassword = async function(enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password)
}

module.exports = mongoose.model('users', User)
