const mongoose = require('mongoose')

const Transaction = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      required: [true, 'transaction must be doing by a user']
    },
    total: {
      type: Number,
      default: 0
    },
    status: {
      type: String,
      enum: ['waited', 'payed', 'sended', 'successed', 'canceled'],
      default: 'waited'
    },
    paymentFile: {
      type: String,
      default: null
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('transactions', Transaction)
