const mongoose = require('mongoose')

const Product = mongoose.Schema(
  {
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'categories',
      required: [true, 'please select a category']
    },
    name: {
      type: String,
      required: [true, 'please insert the product name']
    },
    price: {
      type: Number,
      required: [true, 'please insert the product price']
    },
    size: {
      type: [String],
      required: [true, 'please insert at least one size']
    },
    description: {
      type: String,
      required: [true, 'please insert a description']
    },
    stock: {
      type: Number,
      required: [true, 'please insert the product stock']
    },
    rating: {
      type: Number,
      default: 0
    },
    sold: {
      type: Number,
      default: 0
    },
    active: {
      type: Boolean,
      default: true
    },
    discount: Number,
    viewed: {
      type: Number,
      default: 0
    }
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

//cascade delete product images
Product.pre('remove', async function(next) {
  await this.model('productimages').deleteMany({
    productId: this._id
  })
  next()
})

//generate product images
Product.virtual('images', {
  ref: 'productimages',
  localField: '_id',
  foreignField: 'productId',
  justOne: false
})

//generate one product image
Product.virtual('image', {
  ref: 'productimages',
  localField: '_id',
  foreignField: 'productId',
  justOne: true
})

module.exports = mongoose.model('products', Product)
