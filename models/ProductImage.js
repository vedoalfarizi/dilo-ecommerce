const mongoose = require('mongoose')

const ProductImages = mongoose.Schema({
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'products',
    required: [true, 'please select a product']
  },
  photo: {
    type: String,
    required: [true, 'please insert the product image']
  },
  description: String
})

module.exports = mongoose.model('productimages', ProductImages)
