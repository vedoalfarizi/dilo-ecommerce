const mongoose = require('mongoose')

const Chat = mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users'
    },
    adminId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      require: [true, 'admin ID can not be null']
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

//cascade delete messages
Chat.pre('remove', async function(next) {
  await this.model('messages').deleteMany({
    chatId: this._id
  })
  next()
})

//generate chat histories
Chat.virtual('histories', {
  ref: 'messages',
  localField: '_id',
  foreignField: 'chatId',
  justOne: false
})

module.exports = mongoose.model('chats', Chat)
