const { createLogger, transports, format } = require('winston')
const morgan = require('morgan')
const { APP_ENV } = require('../config/app')

const logger = createLogger({
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.File({
      filename: './logs/combined.log',
      level: 'info',
      json: false,
      maxsize: 5242880,
      maxFiles: 5
    }),
    new transports.Console()
  ],
  exceptionHandlers: [new transports.File({ filename: './logs/exeption.log' })]
})

logger.stream = {
  write: message => logger.info(message.substring(0, message.lastIndexOf('\n')))
}

let predefinedFormat
if (APP_ENV === 'development') {
  predefinedFormat =
    ':method :url :status :response-time ms - :res[content-length]'
} else if (APP_ENV === 'production') {
  predefinedFormat = `:remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]
  `
}

const requestLogger = morgan(predefinedFormat, {
  stream: logger.stream
})

module.exports = {
  logger,
  requestLogger
}
