const router = require('express').Router()
const { get, getProducts } = require('../controllers/category')

router.get('/categories', get)
router.get('/categories/:categoryId/products', getProducts)

module.exports = router
