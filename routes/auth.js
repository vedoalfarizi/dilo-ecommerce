const router = require('express').Router()
const { registerSchema } = require('../schema')
const schemaMiddleware = require('../middlewares/schemaMiddleware')
const { register, login, logout } = require('../controllers/auth')

router.post('/register', schemaMiddleware(registerSchema), register)
router.post('/login', login)
router.get('/logout', logout)

module.exports = router
