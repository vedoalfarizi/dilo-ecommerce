const router = require('express').Router()
const {
  get,
  getDetail,
  updateStatus
} = require('../../controllers/admin/transaction')

router.get('/transactions/', get)
router.get('/transactions/:transactionId', getDetail)
router.put('/transactions/:transactionId/status', updateStatus)

module.exports = router
