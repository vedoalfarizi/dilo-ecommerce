const router = require('express').Router()
const { get, store, update } = require('../../controllers/admin/profile')

router.get('/profiles', get)
router.post('/profiles', store)
router.put('/profiles', update)

module.exports = router