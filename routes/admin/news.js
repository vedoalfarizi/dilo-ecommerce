const router = require('express').Router()
const schemaMiddleware = require('../../middlewares/schemaMiddleware')
const { storeNewsSchema } = require('../../schema')
const {
  get,
  store,
  find,
  update,
  destroy
} = require('../../controllers/admin/news')

router.get('/news', get)
router.post('/news', schemaMiddleware(storeNewsSchema), store)
router.get('/news/:newsId', find)
router.put('/news/:newsId', update)
router.delete('/news/:newsId', destroy)

module.exports = router
