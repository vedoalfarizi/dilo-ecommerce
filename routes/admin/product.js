const router = require('express').Router()

const {
  get,
  find,
  store,
  addProductImage,
  update,
  addStock,
  updateStatus,
  updateDiscount,
  destroy
} = require('../../controllers/admin/product')

router.get('/products', get)
router.get('/products/:productId', find)
router.post('/products', store)
router.post('/products/:productId/images', addProductImage)
router.put('/products/:productId', update)
router.put('/products/:productId/stock', addStock)
router.put('/products/:productId/status', updateStatus)
router.put('/products/:productId/discount', updateDiscount)
router.delete('/products/:productId', destroy)

module.exports = router
