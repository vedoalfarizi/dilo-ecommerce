const router = require('express').Router()
const schemaMiddleware = require('../../middlewares/schemaMiddleware')
const { storePromoSchema } = require('../../schema')
const {
  get,
  store,
  updatePeriod,
  setHighlight,
  destroy
} = require('../../controllers/admin/promo')

router.get('/promos', get)
router.post('/promos', schemaMiddleware(storePromoSchema), store)
router.put('/promos/:promoId/period', updatePeriod)
router.put('/promos/:promoId/highlight', setHighlight)
router.delete('/promos/:promoId', destroy)

module.exports = router
