const router = require('express').Router()

const {
  getRoot,
  getList,
  store,
  storeSub,
  rename,
  destroy
} = require('../../controllers/admin/category')

router.get('/categories', getRoot)
router.get('/list-categories', getList)
router.post('/categories', store)
router.post('/categories/:categoryId', storeSub)
router.put('/categories/:categoryId', rename)
router.delete('/categories/:categoryId', destroy)

module.exports = router
