const router = require('express').Router()
const { destroy } = require('../../controllers/admin/productImage')

router.delete('/product-images/:imageId', destroy)

module.exports = router
