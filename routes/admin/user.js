const router = require('express').Router()

const { get, find, destroy } = require('../../controllers/admin/user')

router.get('/users', get)
router.get('/users/:userId', find)
router.delete('/users/:userId', destroy)

module.exports = router