const router = require('express').Router()
const { getChatBox, sendMessage } = require('../controllers/chat')

router.post('/chatbox', getChatBox)
router.post('/send-message', sendMessage)

module.exports = router
