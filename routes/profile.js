const router = require('express').Router()
const { get } = require('../controllers/profile')

router.get('/profiles', get)

module.exports = router
