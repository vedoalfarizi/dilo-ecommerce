const router = require('express').Router()
const { get, find } = require('../controllers/promo')

router.get('/promos', get)
router.get('/promos/:promoId', find)

module.exports = router
