const router = require('express').Router()
const { get, find } = require('../controllers/product')

router.get('/products', get)
router.get('/products/:productId', find)

module.exports = router
