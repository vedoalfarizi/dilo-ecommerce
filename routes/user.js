const router = require('express').Router()
const schemaMiddleware = require('../middlewares/schemaMiddleware')
const { updateProfileSchema } = require('../schema')
const {
  update,
  updatePassword,
  getAuthUser,
  getTransactionHistory
} = require('../controllers/user')

router.get('/me', getAuthUser)
router.put('/users/:id', schemaMiddleware(updateProfileSchema), update)
router.put('/users/:id/password', updatePassword)
router.get('/users/:id/transactions', getTransactionHistory)

module.exports = router
