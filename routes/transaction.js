const router = require('express').Router()
const {
  buy,
  uploadPayment,
  updateStatus,
  getTransactionDetail
} = require('../controllers/transaction')

router.post('/transactions', buy)
router.put('/transactions/:transactionId/payment', uploadPayment)
router.put('/transactions/:transactionId/status', updateStatus)
router.get('/transactions/:transactionId/detail', getTransactionDetail)

module.exports = router
