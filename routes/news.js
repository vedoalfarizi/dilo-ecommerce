const router = require('express').Router()
const { get, find, search } = require('../controllers/news')

router.get('/news', get)
router.get('/news/:newsId', find)
router.post('/news/search', search)

module.exports = router
